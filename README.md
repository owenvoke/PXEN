## PXEN [PrianX Encryption Network]

A work-in-progress project that is currently not having any new updates for a while as I'm working on A.RK.

It currently features a login system, the ability to add contacts (which are saved), and some other simple stuff<sup>1</sup>.

###### 1 - [Feature list](https://gitlab.com/PXgamer/PXEN/wikis/features)